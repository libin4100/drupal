# LANGUAGE translation of PROJECT
# Copyright (c) YEAR NAME <EMAIL@ADDRESS>
#
msgid ""
msgstr ""

"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2013-12-17 15:49+0800\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: menu:menu:main-menu:description
msgctxt "menu:main-menu:description"
msgid ""
msgstr ""
"The <em>Main</em> menu is used on many sites to show the major "
"sections of the site, often in a top navigation bar."


#: menu:menu:navigation:description
msgctxt "menu:navigation:description"
msgid ""
msgstr ""
"The <em>Navigation</em> menu contains links intended for site "
"visitors. Links are added to the <em>Navigation</em> menu "
"automatically by some modules."


#: menu:menu:user-menu:description
msgctxt "menu:user-menu:description"
msgid ""
msgstr ""
"The <em>User</em> menu contains links related to the user's account, "
"as well as the 'Log out' link."


#: menu:item:761:title
msgctxt "item:761:title"
msgid "Algelia Project"
msgstr "Algelia项目"


#: menu:item:761:description
msgctxt "item:761:description"
msgid "Algelia Project"
msgstr "Algelia项目"


#: menu:item:762:title
msgctxt "item:762:title"
msgid "Part Rotana Emirate"
msgstr "部分Rotana酒店酋长国"


#: menu:item:762:description
msgctxt "item:762:description"
msgid "Part Rotana Emirate"
msgstr "部分Rotana酒店酋长国"


#: menu:item:764:title
msgctxt "item:764:title"
msgid "Radisson Hotel Canada"
msgstr "丽笙大酒店加拿大"


#: menu:item:764:description
msgctxt "item:764:description"
msgid "Radisson Hotel Canada"
msgstr "丽笙大酒店加拿大"


#: menu:item:763:title
msgctxt "item:763:title"
msgid "Project in Vietnam"
msgstr "项目在越南"


#: menu:item:763:description
msgctxt "item:763:description"
msgid "Project in Vietnam"
msgstr "项目在越南"


#: menu:menu:menu-news:title
msgctxt "menu:menu-news:title"
msgid "News"
msgstr "新闻"


#: menu:item:991:title
msgctxt "item:991:title"
msgid "FAQs"
msgstr "常见问题解答"


#: menu:item:991:description
msgctxt "item:991:description"
msgid "FAQs"
msgstr "常见问题解答"


#: menu:item:992:title
msgctxt "item:992:title"
msgid "News"
msgstr "新闻"


#: menu:item:992:description
msgctxt "item:992:description"
msgid "News"
msgstr "新闻"


#: menu:menu:main-menu:title
msgctxt "menu:main-menu:title"
msgid "Main menu"
msgstr "主菜单"


#: menu:menu:management:title
msgctxt "menu:management:title"
msgid "Management"
msgstr "管理"


#: menu:menu:management:description
msgctxt "menu:management:description"
msgid "The <em>Management</em> menu contains links for administrative tasks."
msgstr "在&lt;em&gt;管理&lt;/ em&gt;的菜单中包含的链接的管理任务。"


#: menu:menu:navigation:title
msgctxt "menu:navigation:title"
msgid "Navigation"
msgstr "导航"


#: menu:menu:user-menu:title
msgctxt "menu:user-menu:title"
msgid "User menu"
msgstr "用户菜单"


#: menu:menu:menu-about-us:title
msgctxt "menu:menu-about-us:title"
msgid "About Us"
msgstr "关于我们"


#: menu:item:422:title
msgctxt "item:422:title"
msgid "About Us"
msgstr "关于我们"


#: menu:item:424:title
msgctxt "item:424:title"
msgid "Exhibition"
msgstr "展览"


#: menu:item:426:title
msgctxt "item:426:title"
msgid "Certificate"
msgstr "证书"


#: menu:item:749:title
msgctxt "item:749:title"
msgid "Contact Us"
msgstr "联系我们"


#: menu:item:751:title
msgctxt "item:751:title"
msgid "View of Factory"
msgstr "查看工厂"


#: menu:menu:menu-support:title
msgctxt "menu:menu-support:title"
msgid "Support"
msgstr "支持"


#: menu:item:753:title
msgctxt "item:753:title"
msgid "Pricing"
msgstr "定价"


#: menu:item:754:title
msgctxt "item:754:title"
msgid "Usage & Maintenance"
msgstr "使用与维护"


#: menu:item:755:title
msgctxt "item:755:title"
msgid "Product Advantage"
msgstr "产品优势"


#: menu:item:756:title
msgctxt "item:756:title"
msgid "Installation Guide"
msgstr "安装指南"


#: menu:item:757:title
msgctxt "item:757:title"
msgid "Application"
msgstr "应用"


#: menu:item:758:title
msgctxt "item:758:title"
msgid "Download"
msgstr "下载"


#: menu:menu:menu-projects:title
msgctxt "menu:menu-projects:title"
msgid "Projects"
msgstr "项目"


#: menu:item:825:title
msgctxt "item:825:title"
msgid "Platinum"
msgstr "铂金"


#: menu:item:825:description
msgctxt "item:825:description"
msgid "Platinum"
msgstr "铂金"


#: menu:item:821:title
msgctxt "item:821:title"
msgid "Quartz Stone"
msgstr "石英石"


#: menu:item:821:description
msgctxt "item:821:description"
msgid "Quartz Stone"
msgstr "石英石"


#: menu:item:822:title
msgctxt "item:822:title"
msgid "Classic"
msgstr "经典"


#: menu:item:822:description
msgctxt "item:822:description"
msgid "Classic"
msgstr "经典"


#: menu:item:823:title
msgctxt "item:823:title"
msgid "Fashion"
msgstr "时尚"


#: menu:item:823:description
msgctxt "item:823:description"
msgid "Fashion"
msgstr "时尚"


#: menu:item:824:title
msgctxt "item:824:title"
msgid "Popular"
msgstr "流行"


#: menu:item:824:description
msgctxt "item:824:description"
msgid "Popular"
msgstr "流行"


#: menu:item:826:title
msgctxt "item:826:title"
msgid "Composite Marble"
msgstr "复合大理石"


#: menu:item:826:description
msgctxt "item:826:description"
msgid "Composite Marble"
msgstr "复合大理石"


#: menu:item:827:title
msgctxt "item:827:title"
msgid "Element"
msgstr "元素"


#: menu:item:827:description
msgctxt "item:827:description"
msgid "Element"
msgstr "元素"


#: menu:item:828:title
msgctxt "item:828:title"
msgid "Style"
msgstr "风格"


#: menu:item:828:description
msgctxt "item:828:description"
msgid "Style"
msgstr "风格"


#: menu:item:829:title
msgctxt "item:829:title"
msgid "Latin"
msgstr "拉丁"


#: menu:item:829:description
msgctxt "item:829:description"
msgid "Latin"
msgstr "拉丁"


#: menu:item:830:title
msgctxt "item:830:title"
msgid "Revolution"
msgstr "革命"


#: menu:item:830:description
msgctxt "item:830:description"
msgid "Revolution"
msgstr "革命"


#: menu:item:831:title
msgctxt "item:831:title"
msgid "Marble"
msgstr "大理石"


#: menu:item:831:description
msgctxt "item:831:description"
msgid "Marble"
msgstr "大理石"


#: menu:item:832:title
msgctxt "item:832:title"
msgid "From China"
msgstr "来自中国"


#: menu:item:832:description
msgctxt "item:832:description"
msgid "From China"
msgstr "来自中国"


#: menu:item:833:title
msgctxt "item:833:title"
msgid "Imported"
msgstr "进口"


#: menu:item:833:description
msgctxt "item:833:description"
msgid "Imported"
msgstr "进口"


#: menu:item:834:title
msgctxt "item:834:title"
msgid "Granite"
msgstr "花岗岩"


#: menu:item:834:description
msgctxt "item:834:description"
msgid "Granite"
msgstr "花岗岩"


#: menu:item:835:title
msgctxt "item:835:title"
msgid "From China"
msgstr "来自中国"


#: menu:item:835:description
msgctxt "item:835:description"
msgid "From China"
msgstr "来自中国"


#: menu:item:836:title
msgctxt "item:836:title"
msgid "Imported"
msgstr "进口"


#: menu:item:836:description
msgctxt "item:836:description"
msgid "Imported"
msgstr "进口"


#: menu:item:837:title
msgctxt "item:837:title"
msgid "Slabs"
msgstr "砖"


#: menu:item:837:description
msgctxt "item:837:description"
msgid "Slabs"
msgstr "砖"


#: menu:item:838:title
msgctxt "item:838:title"
msgid "Tiles"
msgstr "瓷砖"


#: menu:item:838:description
msgctxt "item:838:description"
msgid "Tiles"
msgstr "瓷砖"


#: menu:item:839:title
msgctxt "item:839:title"
msgid "Kitchen top"
msgstr "厨房顶部"


#: menu:item:839:description
msgctxt "item:839:description"
msgid "Kitchen top"
msgstr "厨房顶部"


#: menu:item:840:title
msgctxt "item:840:title"
msgid "Window Sill"
msgstr "窗台"


#: menu:item:840:description
msgctxt "item:840:description"
msgid "Window Sill"
msgstr "窗台"


#: menu:item:841:title
msgctxt "item:841:title"
msgid "Waterjet"
msgstr "水刀"


#: menu:item:841:description
msgctxt "item:841:description"
msgid "Waterjet"
msgstr "水刀"


#: menu:item:842:title
msgctxt "item:842:title"
msgid "Promotion"
msgstr "提升"


#: menu:item:842:description
msgctxt "item:842:description"
msgid "Promotion"
msgstr "提升"


#: menu:item:843:title
msgctxt "item:843:title"
msgid "Sample"
msgstr "样品"


#: menu:item:843:description
msgctxt "item:843:description"
msgid "Sample"
msgstr "样品"


