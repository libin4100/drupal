<?php 
/**
 *@author 朽木
 *@ 2013
 * http://www.xiumu.org/technology/google-translate-api-php-translator-class.shtml
 */

class GoogleTranslator
{
	var $snoopy;
	var $result;
	var $text = '这是一个基于Google在线翻译的工具！';
	var $from = 'zh-CN';
	var $to = 'en';
	var $GoogleURL = 'http://translate.google.cn/translate_t';
	var $err;
	
	//php4构造函数
	function GoogleTranslate($text, $from, $to)
	{
		$this->__construct($text, $from, $to);
	}
	
	//php5构造函数
	function __construct($text='', $from='', $to='')
	{
		if(!class_exists('Snoopy'))
			require_once ( dirname(__FILE__).'/Snoopy.class.php' );
		
		$this->snoopy = new Snoopy();
		
		$this->snoopy->agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11';
		//$this->snoopy->accept= 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
		$this->snoopy->referer = 'http://translate.google.cn/';
		//$this->snoopy->rawheaders['Accept-Language'] = 'zh-cn,zh;q=0.5';
		
		$text && $this->text = $text;
		$from && $this->from = $from;
		$to && $this->to = $to;
		
		$this->text && $this->translate();
		
	}
	
	function setLang()
	{
		$Lang = array(
			"中文(繁体)"=>"zh-TW",
			"中文(简体)"=>"zh-CN",
			"阿尔巴尼亚语"=>"sq",
			"阿拉伯语"=>"ar",
			"爱尔兰语"=>"ga",
			"爱沙尼亚语"=>"et",
			"白俄罗斯语"=>"be",
			"保加利亚语"=>"bg",
			"冰岛语"=>"is",
			"波兰语"=>"pl",
			"波斯语"=>"fa",
			"布尔文(南非荷兰语)"=>"af",
			"丹麦语"=>"da",
			"德语"=>"de",
			"俄语"=>"ru",
			"法语"=>"fr",
			"菲律宾语"=>"tl",
			"芬兰语"=>"fi",
			"海地克里奥尔语 ALPHA"=>"ht",
			"韩语"=>"ko",
			"荷兰语"=>"nl",
			"加利西亚语"=>"gl",
			"加泰罗尼亚语"=>"ca",
			"捷克语"=>"cs",
			"克罗地亚语"=>"hr",
			"拉脱维亚语"=>"lv",
			"立陶宛语"=>"lt",
			"罗马尼亚语"=>"ro",
			"马耳他语"=>"mt",
			"马来语"=>"ms",
			"马其顿语"=>"mk",
			"挪威语"=>"no",
			"葡萄牙语"=>"pt",
			"日语"=>"ja",
			"瑞典语"=>"sv",
			"塞尔维亚语"=>"sr",
			"斯洛伐克语"=>"sk",
			"斯洛文尼亚语"=>"sl",
			"斯瓦希里语"=>"sw",
			"泰语"=>"th",
			"土耳其语"=>"tr",
			"威尔士语"=>"cy",
			"乌克兰语"=>"uk",
			"西班牙语"=>"es",
			"希伯来语"=>"iw",
			"希腊语"=>"el",
			"匈牙利语"=>"hu",
			"意大利语"=>"it",
			"意第绪语"=>"yi",
			"印地语"=>"hi",
			"印尼语"=>"id",
			"英语"=>"en",
			"越南语"=>"vi",
		);

		if(!in_array($this->from, $Lang) || !in_array($this->to, $Lang))
		{
			return false;
		}
		return true;
	}
	
	function translate($text='', $from='', $to='')
	{
		$text && $this->text = $text;
		$from && $this->from = $from;
		$to && $this->to = $to;
		if (!$this->setLang()) {
			//语言错误
			$this->err = '语言错误';
			return false;
		}
		
		//目前可用的提交方式
		$submit_vars["hl"] = "zh-CN";
		$submit_vars["text"] = $this->text;
		$submit_vars["ie"] = "UTF8";
		$submit_vars["langpair"] = $this->from . '|' . $this->to;
		
		if(!$this->text)
		{
			$this->result = NULL;
			return NULL;
		}
		
		$this->snoopy->submit($this->GoogleURL,$submit_vars);
		
		if($this->snoopy->status >= 200 && $this->snoopy->status < 300){
			$htmlret = preg_replace('/\<br(\s*)?\/?\>/i', "\n", $this->snoopy->results);
			if(preg_match('/<div.*?id\s*=\s*("|\')?\s*result_box\s*("|\')?.*?>/ius', $htmlret, $matchs) == 1){
				$out = explode($matchs[0],$htmlret);
				unset($matchs);
				$out = explode('</div>',$out[1]);
				$content_tmp = $this->strip_all_tags($out[0]);
				unset($out,$htmlret);
				
				$this->result =  $content_tmp;
				unset($content_tmp);
			}
		}else{
			//没有获取结果 (可能 1 服务器被墙 2 Google改版)
			return false;
		}
		
		return $this->result;
	}
	
	function strip_all_tags($string, $remove_breaks = false) {
		$string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );
		$string = strip_tags($string);

		if ( $remove_breaks )
			$string = preg_replace('/[\r\n\t ]+/', ' ', $string);

		return trim( $string );
	}
	
}
