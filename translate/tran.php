<?php
set_time_limit(0);
error_reporting(E_ALL);
include('GoogleTranslator.class.php');
$lang = array('zh-CN');
//$lang = array('fr', 'de', 'it', 'ru', 'es', 'pt', 'ar');
$idx = 0;
foreach($lang as $l) {
    foreach(glob("*.po") as $e) {
        $str = file_get_contents($e);
        $str = preg_replace("/^msgstr.*/m", '', $str);
        $m = array();
        preg_match_all("/^msgid \"(.+)\"/m", $str, $m);
        foreach($m as $k=>$em) {
            $em = array_unique($em);
            $tmp = array();
            foreach($em as $v) {
                $tmp[] = $v;
            }
            $m[$k] = $tmp;
        }

        $g = new GoogleTranslator(implode("\n",$m[1]), 'en', $l);
        $ret = $g->translate();
        if ($ret==false) {
            die( $g->err );
        }
        $r = explode("\n", $ret);
        $rep = $pat = array();
        foreach($r as $k=>$er) {
            //$rep[$k] = $m[0][$k] . "\nmsgstr \"$er({$m[1][$k]})\""; //For CN
            $rep[$k] = $m[0][$k] . "\nmsgstr \"$er({$m[1][$k]})\"";
        }

        /*
        foreach($m[0] as $k=>$em) {
            $pat[$k] = quotemeta($em) . "/";
            //echo $k . ': ' . $pat[$k] . "\n";
        }
         */
        $new = str_replace($m[0], $rep, $str);
        $new = str_replace('msgid ""', "msgid \"\"\nmsgstr \"\"", $new);
        file_put_contents("res/$l.$e", $new);
        ++$idx;
    }
}
echo $idx;
