<?php
set_time_limit(0);
$st = microtime(true);
$db = new PDO("mysql:host=localhost;dbname=drupal", 'root', '');
$_i = $_GET['i'] ? (int)$_GET['i'] : 1;
$time = time();
$pro = array(
    1 => 'Radisson Hotel Canada',
    'Project in Vietnam',
    'Part Rotana Emirate',
    'Algelia Project'
);
$tArr = array(1 => 30, 29, 28, 27);

if(!isset($_GET['a'])) {
    $_p = str_replace(' ', '-', $pro[$_i]);
    for($i = 1; $i < 11; ++$i) {
        $url = "http://www.quartzstone.com.cn/Projects/$_p-$i.htm";
        $f = file_get_contents($url);
        preg_match('/<img id="Img".*?src="(.*?)"/s', $f, $m);
        $name = end(explode('/', $m[1]));
        copy('http://www.quartzstone.com.cn' . $m[1], '1/' . $name);

        $cat = trim($pro[$_i]);
        $_c = strtolower($cat);
        $title = $cat . ' ' . $i;

        $stm = $db->prepare("insert into node (nid, vid, type, language, title, uid, status, created, changed, comment, promote, sticky, tnid, translate) values (NULL, 0, 'article', 'und', :title, 1, 1, :time, :time, 1, 0, 0, 0, 0)");
        $stm->bindParam(':title', $title);
        $stm->bindParam(':time', $time);
        $stm->execute();
        $nid = $db->lastInsertId();
        $stm = $db->prepare("insert into node_comment_statistics (nid, cid, last_comment_timestamp, last_comment_uid) values (:nid, 0, :time, 1)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':time', $time);
        $stm->execute();
        $stm = $db->prepare("insert into node_revision (nid, vid, uid, title, log, timestamp, status, comment, promote, sticky) values (:nid, NULL, 1, :title, '', :time, 1, 1, 0, 0)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':title', $title);
        $stm->bindParam(':time', $time);
        $stm->execute();
        $vid = $db->lastInsertId();
        $stm = $db->prepare("update node set vid = :vid where nid = :nid");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->execute();

        $img = getimagesize('1/' . $name);
        $size = filesize('1/' . $name);
        $stm = $db->prepare("insert into file_managed (fid, uid, filename, uri, filemime, filesize, status, timestamp) values (NULL, 1, :name, :uri, :mime, :size, 1, :time)");
        $uri = "public://$name";
        $stm->bindParam(':name', $name);
        $stm->bindParam(':uri', $uri);
        $stm->bindParam(':mime', $img['mime']);
        $stm->bindParam(':size', $size);
        $stm->bindParam(':time', $time);
        $stm->execute();
        $fid = $db->lastInsertId();
        $stm = $db->prepare("isnert into file_usage (fid, module, type, id, count) values (':fid', 'file', 'node', ':nid', 1)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':fid', $fid);
        $stm->execute();
        $stm = $db->prepare("insert into field_data_uc_product_image (entity_type, bundle, entity_id, revision_id, language, delta, uc_product_image_fid, uc_product_image_alt, uc_product_image_title, uc_product_image_width, uc_product_image_height) values ('node', 'product', ':nid', ':vid', 'und', 0, ':fid', ':name', ':name', ':width', ':height')");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':fid', $fid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':name', $title);
        $stm->bindParam(':width', $img[0]);
        $stm->bindParam(':height', $img[1]);
        $stm->execute();

        $stm = $db->prepare("insert into field_data_field_image (entity_type, bundle, entity_id, revision_id, language, delta, field_image_fid, field_image_alt, field_image_title, field_image_width, field_image_height) values ('node', 'article', :nid, :vid, 'und', 0, :fid, :alt, :alt, :width, :height)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':fid', $fid);
        $stm->bindParam(':alt', $title);
        $stm->bindParam(':width', $img[0]);
        $stm->bindParam(':height', $img[1]);
        $stm->execute();
        $stm = $db->prepare("insert into field_revision_field_image (entity_type, bundle, entity_id, revision_id, language, delta, field_image_fid, field_image_alt, field_image_title, field_image_width, field_image_height) values ('node', 'article', :nid, :vid, 'und', 0, :fid, :alt, :alt, :width, :height)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':fid', $fid);
        $stm->bindParam(':alt', $title);
        $stm->bindParam(':width', $img[0]);
        $stm->bindParam(':height', $img[1]);
        $stm->execute();
        $stm = $db->prepare("insert into field_data_field_type (entity_type, bundle, entity_id, revision_id, language, delta, field_type_tid) values ('node', 'article', :nid, :vid, 'und', 0, :fid)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':fid', $tArr[$_i]);
        $stm->execute();
        $stm = $db->prepare("insert into field_revision_field_type (entity_type, bundle, entity_id, revision_id, language, delta, field_type_tid) values ('node', 'article', :nid, :vid, 'und', 0, :fid)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':fid', $tArr[$_i]);
        $stm->execute();

        $db->exec("insert into url_alias (pid, source, alias, language) values (NULL, 'node/$nid', 'projects/$nid.htm', 'und')");
        $db->exec("insert into taxonomy_index (nid, tid, sticky, created) values ('$nid', '{$tArr[$_i]}', 0, '$time')");

    /*
    ++$i;
    if(!isset($_GET['stop']) && $i < 7) {
        echo <<<EOT
<script>setTimeout('window.location.href="3.php?i=$i"', 2000)</script>
EOT;
    }
     */
    }
    echo (microtime(true) - $st);
}
