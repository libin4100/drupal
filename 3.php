<?php
set_time_limit(0);
$st = microtime(true);
$db = new PDO("mysql:host=localhost;dbname=drupal", 'root', '');
$i = $_GET['i'] ? (int)$_GET['i'] : 1;
$time = time();
$tax = array('faqs' => 44, 'news' => 46);

if(!isset($_GET['a'])) {
    $url = ($i == 1) ? 'http://www.quartzstone.com.cn/newscenter.htm' : "http://www.quartzstone.com.cn/newscenter$i.htm";
    $f = file_get_contents($url);
    preg_match('/<ul id="news_list">(.*?)<\/ul>/s', $f, $tm);
    preg_match_all('/<a href="([^"]+)".*>/', $tm[1], $m);

    foreach($m[1] as $k => $_m) {
        $_url = 'http://www.quartzstone.com.cn' . $_m;
        $news = file_get_contents($_url);
        preg_match('/<div id="news">(.*?)<div id="footer">/s', $news, $_n);
        preg_match('/<p class="title">([^<]*)/', $_n[1], $c);
        $cat = trim($c[1]);
        $_c = strtolower($cat);
        $tid = $tax[$_c];
        preg_match('/<h1.*?>([^<]*)/', $_n[1], $t);
        $title = trim($t[1]);
        preg_match('/<span class="info_time">([^<]*)/', $_n[1], $t);
        $ctime = trim($t[1]);
        $ts = strtotime($ctime);
        preg_match('/<p>(.*?)<\/p>/s', $_n[1], $c);
        $content = trim(strip_tags($c[1]));

        $stm = $db->prepare("insert into node (nid, vid, type, language, title, uid, status, created, changed, comment, promote, sticky, tnid, translate) values (NULL, 0, 'article', 'en', :title, 1, 1, :time, :time, 1, 0, 0, 0, 0)");
        $stm->bindParam(':title', $title);
        $stm->bindParam(':time', $ts);
        $stm->execute();
        $nid = $db->lastInsertId();
        $stm = $db->prepare("insert into node_comment_statistics (nid, cid, last_comment_timestamp, last_comment_uid) values (:nid, 0, :time, 1)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':time', $ts);
        $stm->execute();
        $stm = $db->prepare("insert into node_revision (nid, vid, uid, title, log, timestamp, status, comment, promote, sticky) values (:nid, NULL, 1, :title, '', :time, 1, 1, 0, 0)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':title', $title);
        $stm->bindParam(':time', $ts);
        $stm->execute();
        $vid = $db->lastInsertId();
        $stm = $db->prepare("update node set vid = :vid where nid = :nid");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->execute();

        $stm = $db->prepare("insert into field_data_field_news_faqs_ (entity_type, bundle, entity_id, revision_id, language, delta, field_news_faqs__tid) values ('node', 'article', :nid, :vid, 'und', 0, :tid)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':tid', $tid);
        $stm->execute();
        $stm = $db->prepare("insert into field_revision_field_news_faqs_ (entity_type, bundle, entity_id, revision_id, language, delta, field_news_faqs__tid) values ('node', 'article', :nid, :vid, 'und', 0, :tid)");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':tid', $tid);
        $stm->execute();

        $stm = $db->prepare("insert into field_data_body (entity_type, bundle, entity_id, revision_id, language, delta, body_value, body_summary, body_format) values ('node', 'article', :nid, :vid, 'und', 0, :content, '', 'filtered_html')");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':content', $content);
        $stm->execute();
        $stm = $db->prepare("insert into field_revision_body (entity_type, bundle, entity_id, revision_id, language, delta, body_value, body_summary, body_format) values ('node', 'article', :nid, :vid, 'und', 0, :content, '', 'filtered_html')");
        $stm->bindParam(':nid', $nid);
        $stm->bindParam(':vid', $vid);
        $stm->bindParam(':content', $content);
        $stm->execute();

        $db->exec("insert into url_alias (pid, source, alias, language) values (NULL, 'node/$nid', '$_c/$nid.htm', 'en')");
        $db->exec("insert into taxonomy_index (nid, tid, sticky, created) values ('$nid', '$tid', 0, '$ts')");
    }

    echo (microtime(true) - $st);
    /*
    ++$i;
    if(!isset($_GET['stop']) && $i < 7) {
        echo <<<EOT
<script>setTimeout('window.location.href="3.php?i=$i"', 2000)</script>
EOT;
    }
     */
}
