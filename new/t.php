<?php
$s = file_get_contents('t.xml');
print_r(new SimpleXMLElement($s));
//echo (post($s));


function post($xml) {
    $url = 'http://test.eshipper.com/eshipper/rpc2';

    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return result (XML) rather than TRUE/FALSE
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

    $xmlResponse=curl_exec($ch);

    $err_no = curl_errno($ch);
    $err_desc = curl_error($ch);
    echo $err_desc;

    if(FALSE == $xmlResponse) {
        return 'no';
    }

    curl_close ($ch);

    return $xmlResponse;
}
