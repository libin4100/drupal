<?php
set_time_limit(0);
$db = new PDO("mysql:host=localhost;dbname=drupal", 'root', '');

$res = $db->query("select * from aa");
$res->setFetchMode(PDO::FETCH_ASSOC);
while($rs = $res->fetch()) {
    $rs = array_map('esc', $rs);

    $time = time();

    $db->exec("insert into node (nid, vid, type, language, title, uid, status, created, changed, comment, promote, sticky, tnid, translate) values (NULL, 0, 'product', 'en', '{$rs['name']}', 1, 1, $time, $time, 1, 0, 0, 0, 0)");
    $nid = $db->lastInsertId();
    $db->exec("insert into node_comment_statistics (nid, cid, last_comment_timestamp, last_comment_uid) values ('$nid', 0, '$time', 1)");
    $db->exec("insert into node_revision (nid, vid, uid, title, log, timestamp, status, comment, promote, sticky) values ($nid, NULL, 1, '{$rs['name']}', '', $time, 1, 1, 0, 0)");
    $vid = $db->lastInsertId();
    $db->exec("update node set vid = '$vid' where nid = '$nid'");

    $uni = md5(uniqid());
    list(, $model) = explode(':', $rs['field3']);
    $model = trim($model);
    $db->exec("insert into uc_products (vid, nid, model, list_price, cost, sell_price, weight, weight_units, length, width, height, length_units, pkg_qty, default_qty, unique_hash, ordering, shippable) values ('$vid', '$nid', '$model', 0, 0, 0, 0, 'lb', 0, 0, 0, 'in', 1, 1, '$uni', 0, 0)");

    $img = getimagesize('1/' . $rs['nimg']);
    $size = filesize('1/' . $rs['nimg']);
    $db->exec("insert into file_managed (fid, uid, filename, uri, filemime, filesize, status, timestamp) values (NULL, 1, '{$rs['nimg']}', 'public://{$rs['nimg']}', '{$img['mime']}', $size, 1, $time)");
    $fid = $db->lastInsertId();
    $db->exec("isnert into file_usage (fid, module, type, id, count) values ('$fid', 'file', 'node', '$nid', 1)");
    $db->exec("insert into field_data_uc_product_image (entity_type, bundle, entity_id, revision_id, language, delta, uc_product_image_fid, uc_product_image_alt, uc_product_image_title, uc_product_image_width, uc_product_image_height) values ('node', 'product', '$nid', '$vid', 'und', 0, '$fid', '{$rs['name']}', '{$rs['name']}', '{$img[0]}', '{$img[1]}')");
    $db->exec("insert into field_revision_uc_product_image (entity_type, bundle, entity_id, revision_id, language, delta, uc_product_image_fid, uc_product_image_alt, uc_product_image_title, uc_product_image_width, uc_product_image_height) values ('node', 'product', '$nid', '$vid', 'und', 0, '$fid', '{$rs['name']}', '{$rs['name']}', '{$img[0]}', '{$img[1]}')");

    $rs['url'] = substr($rs['url'], 1);
    $db->exec("insert into url_alias (pid, source, alias, language) values (NULL, 'node/$nid', '{$rs['url']}', 'en')");

    for($i = 2; $i < 10; ++$i) {
        if($i == 3) continue;
        $k = 'field' . $i;
        list($key, $val) = explode(':', $rs[$k]);
        $v = array_map('trim', explode(',', $val));
        $val = array_shift($v);

        $key = strtolower(str_replace(' ', '_', $key));
        $table = 'field_data_field_' . $key;
        $tableR = 'field_revision_field_' . $key;
        $field = 'field_' . $key . '_value';
        $db->exec("insert into $table (entity_type, bundle, entity_id, revision_id, language, delta, $field) values ('node', 'product', '$nid', '$vid', 'und', 0, '$val')");
        $db->exec("insert into $tableR (entity_type, bundle, entity_id, revision_id, language, delta, $field) values ('node', 'product', '$nid', '$vid', 'und', 0, '$val')");
        if(count($v)) {
            foreach($v as $_k => $_v) {
                $_i = $_k + 1;
                $db->exec("insert into $table (entity_type, bundle, entity_id, revision_id, language, delta, $field) values ('node', 'product', '$nid', '$vid', 'und', $_i, '$_v')");
                $db->exec("insert into $tableR (entity_type, bundle, entity_id, revision_id, language, delta, $field) values ('node', 'product', '$nid', '$vid', 'und', $_i, '$_v')");
            }
        }
    }

    list(, $cat) = explode(':', $rs['field1']);
    $cat = trim($cat);
    $cata = empty($rs['catagory']) ? $cat : $rs['catagory'];
    foreach($db->query("select tid from taxonomy_term_data where name = '$cata'", PDO::FETCH_ASSOC) as $_rs) {
        $tid = $_rs['tid'];
    }
    $db->exec("insert into field_data_taxonomy_catalog (entity_type, bundle, entity_id, revision_id, language, delta, taxonomy_catalog_tid) values ('node', 'product', '$nid', '$vid', 'und', 0, '$tid')");
    $db->exec("insert into field_revision_taxonomy_catalog (entity_type, bundle, entity_id, revision_id, language, delta, taxonomy_catalog_tid) values ('node', 'product', '$nid', '$vid', 'und', 0, '$tid)'");
    $db->exec("insert into taxonomy_index (nid, tid, sticky, created) values ('$nid', '$tid', 0, '$time')");

    $db->exec("insert into field_data_body (entity_type, bundle, entity_id, revision_id, language, delta, body_value, body_summary, body_format) values ('node', 'product', '$nid', '$vid', 'und', 0, '{$rs['field10']}', '', 'full_html')");
    $db->exec("insert into field_revision_body (entity_type, bundle, entity_id, revision_id, language, delta, body_value, body_summary, body_format) values ('node', 'product', '$nid', '$vid', 'und', 0, '{$rs['field10']}', '', 'full_html')");

}

function esc($str) {
    return str_replace("'", "\'", $str);
};
